﻿using Microsoft.Extensions.Configuration;

namespace Falcon.Cache.Test
{
    /// <summary>
    /// RedisCache工厂
    /// </summary>
    public static class RedisCacheFactory
    {
        /// <summary>
        /// 创建RedisCache对象
        /// </summary>
        /// <returns></returns>
        public static IStringCache Create() {
            ConfigurationBuilder builder = new();
            builder.AddJsonFile("AppSettings.json");
            var config = builder.Build();
            var redisOption = config.GetSection("RedisCache").Get<RedisCacheOption>();
            var redis = new RedisCache(redisOption);
            return redis as IStringCache;
        }
    }

}
