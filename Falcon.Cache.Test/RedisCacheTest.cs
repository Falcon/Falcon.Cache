using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Falcon.Cache.Test
{
    [TestClass]
    public class RedisCacheTest
    {
        [TestMethod]
        public void CacheStringTest1() {
            //地区配置实例化缓存服务器
            using var cache = RedisCacheFactory.Create();
            //开始测试功能
            Console.WriteLine("开始服务器可用性测试！");
            Assert.IsTrue(cache != null,"没有正确实例化服务器");
            Console.WriteLine("开始一次存取测试");
            cache.SetCache("abc","abc",TimeSpan.FromSeconds(2));
            var v = cache.GetCache("abc");
            Assert.IsTrue(v == "abc","读取数值失败");
            Thread.Sleep(3 * 1000);
            v = cache.GetCache("abc");
            Assert.IsTrue(v == null,"超时后释放数据失败");
            Console.WriteLine("重复读取测试");
            cache.SetCache("abc","abc",TimeSpan.FromMinutes(1));
            v = cache.GetCache("abc");
            Thread.Sleep(5 * 1000);
            v = cache.GetCache("abc");
            Assert.IsTrue(v == "abc","重复读取失败");
            Console.WriteLine("多次读取测试");
            cache.SetCache("abc","abc",TimeSpan.FromHours(1));
            Stopwatch watch = new();
            watch.Start();
            for(int i = 0;i < 1 * 1000;i++) {
                v = cache.GetCache("abc");
            }
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalSeconds);
        }

        [TestMethod]
        public void CacheObjectTest() {
            var obj = TestObj.GetOne();
            using var cache = RedisCacheFactory.Create();
            var key = "abc";
            var span = new TimeSpan(0,1,0);
            cache.SetCache(key,obj,span);
            var go1 = cache.GetCache<TestObj>(key);
            Assert.IsTrue(obj.Equals(go1));
            Thread.Sleep(10);
            var go2 = cache.GetCache<TestObj>(key);
            Assert.IsTrue(obj.Equals(go2));
        }

        [TestMethod]
        public void CacheObjByObjTest() {
            using var cache = RedisCacheFactory.Create();
            var key1 = new KeyObj { Id = 1,Name = "abc1" };
            var key2 = new KeyObj1 { Id = 2,Name = "abc2" };
            var key3 = new KeyObj2 { Id = 3,Name = "abc3" };
            var obj = TestObj.GetOne();
            var span = new TimeSpan(0,1,0);
            cache.SetCache(key1,obj,span);
            cache.SetCache(key2,obj,span);
            cache.SetCache(key3,obj,span);

            var r1 = cache.GetCache<KeyObj,TestObj>(key1);
            var r2 = cache.GetCache<KeyObj1,TestObj>(key2);
            var r3 = cache.GetCache<KeyObj2,TestObj>(key3);

            Assert.IsTrue(obj.Equals(r1));
            Assert.IsTrue(obj.Equals(r2));
            Assert.IsTrue(obj.Equals(r3));
        }
    }

    public class TestObj
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<TestObj> StrList { get; set; }

        public static TestObj GetOne() {
            return new TestObj {
                Id = 1,
                Name = "name1",
                StrList = new List<TestObj> {
                      new TestObj{ Id=11,Name="name11" },
                      new TestObj{ Id=12,Name="name12" },
                      new TestObj{ Id=13,Name="name13" },
                  },
            };
        }

        public override bool Equals(object obj) {
            if(obj is TestObj i) {
                return i.Id == this.Id && i.Name == this.Name &&
                    i.StrList?.Count() == this.StrList?.Count();
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }

    class KeyObj:IGenerateKey
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string GetCacheKey() {
            return $"KeyObj:Id:Name:{Id}:{Name}";
        }
    }
    class KeyObj1
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
    class KeyObj2
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
