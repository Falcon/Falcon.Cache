﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Falcon.Cache.Test
{
    [TestClass]
    public class IsSerializableTest
    {
        /// <summary>
        /// 测试对象的IsSerializable返回结果
        /// </summary>
        [TestMethod]
        public void GetIsSerializableTest() {
            Console.WriteLine(IsSerializableValue(123));
            Console.WriteLine(IsSerializableValue("123"));
            Console.WriteLine(IsSerializableValue(new DateTime(2000,1,1)));
            Console.WriteLine(IsSerializableValue(new SerObj()));
            Console.WriteLine(IsSerializableValue(new SerObj1()));
            Console.WriteLine(IsSerializableValue(new List<string>()));
            Console.WriteLine(IsSerializableValue((new List<string>()).ToArray()));
            Console.WriteLine(IsSerializableValue(new List<SerObj>()));
            Console.WriteLine(IsSerializableValue(new List<SerObj1>()));
        }

        private string IsSerializableValue(object obj) {
            if(obj == null) {
                return "Null不能序列化";
            }
            var type = obj.GetType();
            return type.IsSerializable ? $"{type.FullName}:可以序列化" : $"{type.FullName}:不能序列化";
        }

        class SerObj
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        [Serializable]
        class SerObj1
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
