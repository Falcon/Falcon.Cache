﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Falcon.Cache.Test
{
    [TestClass]
    public class RedisCacheWithFuncTest
    {
        [TestMethod]
        public void SetGetTest() {
            using var cache = RedisCacheFactory.Create();
            var key = "aaa";
            cache.RemoveCache(key);
            var ts = new TimeSpan(0,0,10);
            var r = cache.GetCache(key);
            Assert.IsTrue(r == null);
            r = cache.GetCache(key,() => {
                return "123";
            });
            Assert.IsTrue(r == "123");
            r = "456";
            r = cache.GetCache(key);
            Assert.IsTrue(r == null);
            r = cache.GetCache(key,() => {
                cache.SetCache(key,"123",ts);
                return "123";
            });
            Assert.IsTrue(r == "123");
            r = "456";
            r = cache.GetCache(key);
            Assert.IsTrue(r == "123");
        }
    }
}
