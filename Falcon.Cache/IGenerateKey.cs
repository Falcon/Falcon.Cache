﻿namespace Falcon.Cache
{
    /// <summary>
    /// 实现创建对象Key的方法
    /// </summary>
    /// <remarks>获取对象Key的时候调用该方法获取用于缓存的Key</remarks>
    public interface IGenerateKey
    {
        /// <summary>
        /// 获取对象的Key，这个Key唯一标示该对象
        /// </summary>
        /// <returns>标示对象的Key</returns>
        string GetCacheKey();
    }
}
