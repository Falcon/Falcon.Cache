﻿using System;

namespace Falcon.Cache
{
    /// <summary>
    /// 没有设置连接字符串的值异常
    /// </summary>
    public class RedisConnectionException:Exception
    {
        /// <summary>
        /// 实例化异常
        /// </summary>
        public RedisConnectionException(string msg) : base(msg) { }
    }
}
