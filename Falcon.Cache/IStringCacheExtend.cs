﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using Falcon.StringExtend;

namespace Falcon.Cache
{
    /// <summary>
    /// 对IStringCache接口进行扩展
    /// </summary>
    public static class IStringCacheExtend
    {
        /// <summary>
        /// 将一个对象进行缓冲
        /// </summary>
        /// <typeparam name="T">对象的类型</typeparam>
        /// <param name="cache">基础缓冲器</param>
        /// <param name="key">缓冲用的key</param>
        /// <param name="obj">要缓冲的对象</param>
        /// <param name="span">缓冲时间</param>
        public static void SetCache<T>(this IStringCache cache,string key,T obj,TimeSpan? span) {
            if(key.IsNullOrEmpty() || obj == null) {
                return;
            }
            var str = JsonSerializer.Serialize(obj);
            cache.SetCache(key,str,span);
        }

        /// <summary>
        /// 从缓存中获取数据
        /// </summary>
        /// <typeparam name="T">要获取的数据类型</typeparam>
        /// <param name="key">数据的键</param>
        /// <param name="cache">基础缓冲器</param>
        /// <returns>存储的对象，如果不存在则为null</returns>
        public static T GetCache<T>(this IStringCache cache,string key) where T : class {
            if(key.IsNullOrEmpty()) {
                return null;
            }
            var str = cache.GetCache(key);
            if(str == null) {
                return null;
            }
            return JsonSerializer.Deserialize<T>(str);
        }

        /// <summary>
        /// 通过将一个对象作为key将另一个对象存入缓存
        /// </summary>
        /// <typeparam name="TKey">原始对象的类型</typeparam>
        /// <typeparam name="TVal">存入缓存对象的类型</typeparam>
        /// <param name="cache">基础缓存器</param>
        /// <param name="key">作为key的对象</param>
        /// <param name="obj">要缓存的对象</param>
        /// <param name="span">缓存时间</param>
        public static void SetCache<TKey, TVal>(this IStringCache cache,TKey key,TVal obj,TimeSpan? span) where TKey : class {
            if(key == null) {
                return;
            }
            cache.SetCache(cache.GetCacheKey(key),obj,span);
        }

        /// <summary>
        /// 通过一个Key对象从缓存中获取缓存对象
        /// </summary>
        /// <typeparam name="TKey">Key对象的类型</typeparam>
        /// <typeparam name="TVal">缓存对象的类型</typeparam>
        /// <param name="cache">基础缓存对象</param>
        /// <param name="key">Key对象</param>
        /// <returns>Key对象对应的缓存的对象</returns>
        public static TVal GetCache<TKey, TVal>(this IStringCache cache,TKey key) where TKey : class where TVal : class {
            if(key == null) {
                return null;
            }
            return cache.GetCache<TVal>(cache.GetCacheKey(key));
        }

        /// <summary>
        /// 通过Key对象创建一个Key
        /// </summary>
        /// <typeparam name="T">Key对象类型</typeparam>
        /// <param name="cache">基础缓冲对象</param>
        /// <param name="key">Key对象</param>
        /// <returns>Key对象对应的Key</returns>
        public static string GetCacheKey<T>(this IStringCache cache,T key) where T : class {
            if(key == null) {
                return null;
            }
            //如果实现接口直接使用接口返回值
            if(key is IGenerateKey igk) {
                return igk.GetCacheKey();
            }
            var sbKey = new StringBuilder();
            var pros = typeof(T).GetProperties()
                .Where(p => p.CanRead && p.PropertyType.IsSerializable)
                .Select(p => new {
                    p,
                    name = p.Name,
                    type = p.PropertyType.FullName,
                    hasKeyAttribute = p.GetCustomAttribute<KeyAttribute>() != null,
                    isClass = p.PropertyType.IsClass,
                });
            //如果使用了KeyAttribute定义则使用Key定义的属性
            if(pros.Any(m => m.hasKeyAttribute)) {
                foreach(var k in pros.Where(m => m.hasKeyAttribute)) {
                    sbKey.Append($"{k.name}:{k.p.GetValue(key)}:");
                }
                return $"{key.GetType().FullName}:bk:{sbKey}";
            }
            //使用全部可序列化爽
            foreach(var k in pros) {
                sbKey.Append($"{k.name}:{k.p.GetValue(key)}:");
            }
            return $"{key.GetType().FullName}:lk:{sbKey}";
        }

        /// <summary>
        /// 从缓存中获取数据,如果缓存返回null，则调用func方法获取数据。
        /// </summary>
        /// <param name="cache">基础缓存器</param>
        /// <param name="key">数据对应的键</param>
        /// <param name="func">如果缓存不存在使用的获取方法</param>
        /// <returns>缓存的对象</returns>
        public static string GetCache(this IStringCache cache,string key,Func<string> func) {
            return cache.GetCache(key) ?? func();
        }

        /// <summary>
        /// 从缓存中获取数据,如果缓存返回null，则调用func方法获取数据。
        /// </summary>
        /// <typeparam name="T">要获取的数据类型</typeparam>
        /// <param name="cache">基础缓存器</param>
        /// <param name="key">数据对应的键</param>
        /// <param name="func">如果缓存不存在使用的获取方法</param>
        /// <returns>缓存的对象</returns>
        public static T GetCache<T>(this IStringCache cache,string key,Func<T> func) where T : class {
            return cache.GetCache<T>(key) ?? func();
        }

        /// <summary>
        /// 从缓存中获取数据,如果缓存返回null，则调用func方法获取数据。
        /// </summary>
        /// <typeparam name="TKey">数据对应键的类型</typeparam>
        /// <typeparam name="TVal">要获取的数据类型</typeparam>
        /// <param name="cache">基础缓存器</param>
        /// <param name="key">数据对应的键</param>
        /// <param name="func">如果缓存不存在使用的获取方法</param>
        /// <returns>缓存的对象</returns>
        public static TVal GetCache<TKey, TVal>(this IStringCache cache,TKey key,Func<TVal> func)
            where TKey : class where TVal : class {
            return cache.GetCache<TKey,TVal>(key) ?? func();
        }


    }
}
