﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Falcon.Cache
{
    /// <summary>
    /// 服务集合扩展
    /// </summary>
    public static class IServiceCollectionExtend
    {
        /// <summary>
        /// 增加字符串缓冲支持
        /// </summary>
        /// <param name="services">服务结合</param>
        /// <param name="optionSection">配置选项</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddStringCacheSupport(this IServiceCollection services,IConfiguration optionSection) {
            services.Configure<RedisCacheOption>(optionSection);
            services.AddSingleton<IStringCache,RedisCache>(sp=> {
                return new RedisCache(sp.GetService<IOptions<RedisCacheOption>>().Value);
            });
            return services;
        }
    }
}
