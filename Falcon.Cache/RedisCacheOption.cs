﻿using System;
using Microsoft.Extensions.Options;

namespace Falcon.Cache
{
    /// <summary>
    /// RedisCache服务器配置
    /// </summary>
    public class RedisCacheOption:IOptions<RedisCacheOption>
    {
        /// <summary>
        /// 获取默认值
        /// </summary>
        public RedisCacheOption Value => this;

        /// <summary>
        /// 默认缓存时间
        /// </summary>
        public TimeSpan DefaultCacheTimespan { get; set; } = new TimeSpan(0,1,0);

        /// <summary>
        /// Redis服务器连接字符串
        /// </summary>
        public string ConnString { get; set; } = null;

        /// <summary>
        /// 默认数据库编号
        /// </summary>
        public int DefaultDatabaseNumber { get; set; } = 0;
    }
}
