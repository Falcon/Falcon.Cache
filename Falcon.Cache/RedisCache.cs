﻿using System;
using Falcon.StringExtend;
using StackExchange.Redis;

namespace Falcon.Cache
{
    /// <summary>
    /// 使用Redis存储缓存
    /// </summary>
    public class RedisCache:IStringCache, IDisposable
    {
        /// <summary>
        /// Redis存储参数
        /// </summary>
        public RedisCacheOption Option { get; set; }
        /// <summary>
        /// Redis连接
        /// </summary>
        public ConnectionMultiplexer Conn { get; private set; }
        /// <summary>
        /// Redis数据库
        /// </summary>
        public IDatabase Database => this.Conn.GetDatabase(Option.DefaultDatabaseNumber);
        /// <summary>
        /// 是否已经释放
        /// </summary>
        public bool IsDisposabled { get; private set; } = false;

        /// <summary>
        /// Redis服务器是否可用
        /// </summary>
        public bool IsAvailable => this.Conn != null && this.Conn.IsConnected && !this.IsDisposabled;

        /// <summary>
        /// 使用参数构造Redis缓存
        /// </summary>
        /// <param name="option">构造参数</param>
        public RedisCache(RedisCacheOption option) {
            this.Option = option;
            if(this.Option.ConnString.IsNullOrEmpty()) {
                throw new RedisConnectionException("没有配置连接字符串，必须正确设置RedisCacheOption实例的ConnString值。");
            }
            this.Conn = ConnectionMultiplexer.Connect(this.Option.ConnString);
            if(!this.Conn.IsConnected) {
                throw new RedisConnectionException("Redis连接失败");
            }
        }

        /// <summary>
        /// 清除所有缓存
        /// </summary>
        public void CleanCache() {
            if(!IsAvailable)
                return;
            try {
                this.Conn.GetServer(Option.ConnString).FlushDatabase(Option.DefaultDatabaseNumber);
            } catch(Exception ex) {
                throw new RedisGetOrSetException("CleanCache发生异常！",ex);
            }
        }
        /// <summary>
        /// 获取缓存数据，如果没有缓存返回null
        /// </summary>
        /// <param name="key">缓存数据的键</param>
        /// <returns>缓存的数据或null</returns>
        public string GetCache(string key) {
            if(!IsAvailable)
                return null;
            try {
                return this.Database.StringGetAsync(key).Result;
            } catch(Exception ex) {
                throw new RedisGetOrSetException(key,"",ex);
            }
        }
        /// <summary>
        /// 删除缓存数
        /// </summary>
        /// <param name="key">缓存数据的键</param>
        /// <returns>是否成功删除数据</returns>
        public bool RemoveCache(string key) {
            if(!IsAvailable)
                return true;
            try {
                return this.Database.KeyDeleteAsync(key).Result;
            } catch(Exception ex) {
                throw new RedisGetOrSetException(key,"",ex);
            }
        }
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">存储数据的键</param>
        /// <param name="str">要存储的字符串</param>
        /// <param name="span">存储时间</param>
        public void SetCache(string key,string str,TimeSpan? span = null) {
            if(!IsAvailable)
                return;
            var _span = span.HasValue ? span.Value : this.Option.DefaultCacheTimespan;
            try {
                this.Database.StringSet(key,str,expiry: _span);
            } catch(Exception ex) {
                throw new RedisGetOrSetException(key,str,ex);
            }
        }

        /// <summary>
        /// 释放占用资源
        /// </summary>
        public void Dispose() {
            if(this.Conn is IDisposable dis) {
                this.Conn.CloseAsync().Wait();
                dis.Dispose();
                this.Conn = null;
                this.IsDisposabled = true;
            }
        }
    }
}
