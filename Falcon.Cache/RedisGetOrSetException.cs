﻿using System;

namespace Falcon.Cache
{
    /// <summary>
    /// Redis数据库存取数据异常
    /// </summary>
    public class RedisGetOrSetException:Exception
    {
        /// <summary>
        /// 实例化异常
        /// </summary>
        public RedisGetOrSetException(string msg,Exception exception) : base(msg,exception) { }

        /// <summary>
        /// 实例化异常
        /// </summary>
        public RedisGetOrSetException(string key,string value,Exception exception) 
            : base($"存储数据发生异常，键：[{key}]值：[{value}]。详情查看内部异常：",exception) { }
    }
}
