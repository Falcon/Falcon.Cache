﻿using System;

namespace Falcon.Cache
{
    /// <summary>
    /// 字符串缓冲
    /// </summary>
    public interface IStringCache:IDisposable
    {
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key">缓存的数据键</param>
        /// <param name="str">要缓存的字符串</param>
        /// <param name="span">缓存时间。null为无限期</param>
        void SetCache(string key,string str,TimeSpan? span);

        /// <summary>
        /// 从缓存中获取数据
        /// </summary>
        /// <param name="key">数据的键</param>
        /// <returns>存储的字符串，如果键不存在则为null</returns>
        string GetCache(string key);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="key">数据的键</param>
        /// <returns>是否成功</returns>
        bool RemoveCache(string key);

        /// <summary>
        /// 清除所有缓存数据
        /// </summary>
        void CleanCache();
    }
}
